#!/bin/bash
#Linus Setup Script
#Fedora
#Add repos
#enable third party
sudo dnf -y install fedora-workstation-repositories
#rpmfusion free
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
#rpmfusion nonfree
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#google chrome
sudo dnf config-manager --set-enabled google-chrome
#flathub
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#update
sudo dnf up -y
#installs
sudo dnf install -y vim google-chrome-stable steam papirus-icon-theme tilix terminator screen tmux minicom gnome-tweaks gnome-firmware cups-pdf system-config-printer bluez-tools blueman zsh zsh-autosuggestions thunderbird cura deluge android-tools nomacs asunder freecad blender gimp inkscape audacity okular openshot nextcloud-client wireshark vlc file-roller p7zip neofetch ffmpegthumbnailer wireguard-tools easytag samba rust tlp power-profiles-daemon WoeUSB libgtop2-devel lm_sensors gnome-extensions-app discord gnome-shell-extension-sound-output-device-chooser gnome-shell-extension-openweather gnome-shell-extension-places-menu gnome-shell-extension-gamemode gnome-shell-extension-gsconnect lpf-spotify-client
sudo flatpak install flathub org.signal.Signal com.bitwarden.desktop
